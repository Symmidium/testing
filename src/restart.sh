#!/bin/bash

cd $1
kill `cat pid.txt`
git pull
nohup python main.py &
echo $! > pid.txt
